#!/bin/bash 

#rm -f hosts.txt #Remove if exists
cd ~/blender

rm -f kill_blenders.sh
rm -f node.log
rm -f kill.log


numnodes=$(wc hosts.txt | awk '{print $1}')
echo Live nodes $numnodes

start=1;
end=100;
let "totalframes=$end-$start+1"
let i=0
framespernode=$(perl -w -e "use POSIX; print ceil($totalframes/$numnodes), qq{\n}")

echo Frames per node $framespernode

for NODE in `cat hosts.txt`;
do
let currstart=i*$framespernode+1
let currend=(i+1)*$framespernode
EXECUTABLE="ssh -f "$NODE" cd ~/blender; ~/blender/blender-2.77a/blender -b ./untitled.blend -E CYCLES --python ./gpurender.py -o ./renders/f_##### -s $currstart  -e $currend -a"
${EXECUTABLE} &> ~/blender/output/$NODE.log && echo $NODE > node.log
if [ $? -eq 0 ]
then
	echo ${EXECUTABLE}
	echo "Killing $NODE" >> kill_blenders.sh
	echo "ssh -f $NODE killall blender" >> kill_blenders.sh
	let i=$i+1
else
	echo "Error connecting to" $NODE
fi
done
chmod +x kill_blenders.sh
