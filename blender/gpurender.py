import bpy
print('*** Compute Device Selection [CUDA: ', 1, ']')
bpy.context.user_preferences.system.compute_device_type = 'CUDA'
bpy.context.user_preferences.system.compute_device = 'CUDA_MULTI_2'
bpy.data.scenes["Scene"].cycles.device = 'GPU'
