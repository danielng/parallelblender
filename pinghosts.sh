#!/bin/bash
rm -f hosts.txt
cat /etc/hosts | awk '{print $2}' | grep host108 > hosts.txt
for NODE in `cat hosts.txt`;
do
EXECUTABLE="ssh -f $NODE -o ConnectTimeout=1 -o BatchMode=yes -o StrictHostKeyChecking=no 'whoami'"
${EXECUTABLE} &> /dev/null
if [ $? -eq 0 ]
   then
   echo $NODE up;
   else
   echo $NODE down;
   sed -i "s/\<$NODE\>//g" hosts.txt
   sed -i "/^\s*$/d" hosts.txt
   echo Removed
fi
done
